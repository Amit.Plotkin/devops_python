#Write a program that asks a user to type their age in years, and prints back the age in months
def ageToMonth(years):
    return int(years)*12 

#Write a program that asks a user to type their age in months, and prints back the age in years.
def monthToYears(month):
    return int(month)/12


#Write a program that asks the user to select a number and prints the word Boom if the number is divisible by 7
def boom7(number):
    if int(number)%7 == 0:
        print("BOOM")
    else:
        print(number)

#Write a program that asks the user to select a number and prints the word Boom if the number is divisible by 7 or contains the number 7.
def contains7(number):
    numberStr=str(number)
    if "7" in numberStr:
        print("BOOM")
        return True
    return False
        
#Write a program that asks the user to choose three numbers and prints the largest one between them.
def Max(number1,number2):
    if int(number1)>int(number2):
        return number1
    else:
        return number2;

#Then write a program that asks the user to select the first organ, the difference and the number of organs in an invoice series and prints the series sum.
def ArithmeticProgression(FirstNumber,difference,itemNumber):
    LastNumber=int(FirstNumber) +(int(itemNumber)-1)*int(difference)
    result=(int(itemNumber)*(int(FirstNumber)+int(LastNumber)))/2
    return result