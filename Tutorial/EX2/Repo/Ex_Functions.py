from random import randint


def helloWorld():
    print("Hello Python")
################################################################
def Ex1():
    IntArr_userInput= Get_input_From_User("please input 10 integer numbers",10)
    int_MaxValue = Get_Max(IntArr_userInput)
    print(f"the max value is: {int_MaxValue}")

################################################################
def Ex2():
    usersAge = Get_input_From_User("please input your Age",1)
    age_in_month = usersAge[0]*12
    print("your age {0} month old".format(age_in_month))

################################################################
def Ex3():
    storage=""
    print("start entering sentences, empty line to end.")
    while True:
        myline = input("insert your next input> ")
        if not myline:
            reverse = Reverse_String(storage)
            print(reverse)
            break
        storage+=myline

################################################################
def Ex4():
    while True:
        myRandomInt =  randint(0,1000000)
        print(myRandomInt)
        if (myRandomInt%7==0) and (myRandomInt%13==0) and (myRandomInt%15==0):
            print(f"the number {myRandomInt} ban be divided by 7,13,15")
            break

################################################################
def Ex5():
    x =  randint(0,10)
    y =  randint(0,10)
    print(f"{x},{y}")
    gcd = GCD(x,y)
    print(gcd)

################################################################
def Ex6():
    print("dice a number between 1-100")
    number = randint(0,100)
    fooluser= randint(0,100)%10==0# fool user once in a while
    print("guess my numbet")
    while True:
        yourChoice=Get_input_From_User("please choose number: ",1)[0]
        
        if fooluser or number>yourChoice:
            print("Grater")
        elif number<yourChoice:
            print("Lower")
        else:
            print("you are correct")
            break


################################################
def Reverse_String(stringInput):
    return stringInput[::-1]
#############################################
def Get_input_From_User(userPrompt,numberOfvalues):

    index=0
    inputCollection=[]
    print(userPrompt)
    while index<numberOfvalues:
        userInput = input("next input>")
        try:
          inputCollection.append(int(userInput))
        except Exception as ex:
            print(ex)
            print(f"the string {userInput} is not a integer number, please try again")
            continue;
        index=index+1
    return inputCollection

###################################################
def Get_Max(int_Array):
    result=-1
    for  value in int_Array:
        if value>result:
            result=value
    return result     
##################################################
def GCD(x, y): 
  
   while(y): 
       x, y = y, x % y 
  
   return x 